package com.baymax.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 管理应用配置的类
 */
public class PropertiesManager {

    private final Properties properties = new Properties();

    public PropertiesManager() {
        /*此方法适用于获取在根目录下的properties文件，具体路径需要根据实际路径决定，
        例如位于根目录下properties文件夹下，则为"properties/config.properties"*/
        InputStream is = PropertiesManager.class.getClassLoader().getResourceAsStream("config.properties");
        try {
            properties.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null)
                    is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public <T> T getProperty(String key) {
        return Utils.forceTrans(properties.get(key));
    }
}