package com.baymax.rest;

import javax.swing.*;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;

public class UI {

    public static CountDownLatch latch;

    public static void setLatch(CountDownLatch countDownLatch) {
        latch = countDownLatch;
    }

    /**
     * 构建消息框
     */
    private static void buildMessageBox(String message) throws UnsupportedLookAndFeelException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        UIManager.setLookAndFeel(new NimbusLookAndFeel());
        // 创建及设置窗口
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setBounds(100, 50, 500, 300);
        frame.setLocationRelativeTo(null);
        frame.setAlwaysOnTop(true);
        frame.setResizable(false);
        frame.setUndecorated(true);// 去掉最大最小关闭按钮
        GridBagLayout layout = new GridBagLayout();
        frame.setLayout(layout);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();//实例化这个对象用来对组件进行管理
        gridBagConstraints.fill = GridBagConstraints.BOTH;//该方法是为了设置如果组件所在的区域比组件本身要大时的显示情况

        // 添加提示信息标签
        JLabel label = new JLabel(message, JLabel.CENTER);
        label.setFont(new Font("微软雅黑", Font.PLAIN, 20));
        gridBagConstraints.gridwidth = 0;
        layout.addLayoutComponent(label, gridBagConstraints);
        frame.getContentPane().add(label);

        // 初始化恢复计时按钮
        JButton button = new JButton("恢复计时");
        button.setFont(new Font("微软雅黑", Font.PLAIN, 20));
        button.setVisible(false);// 初始不显示，不允许操作button
        button.addActionListener(e -> frame.dispose());
        button.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                super.componentShown(e);
                try {
                    SystemEvent.lockScreen();
                } catch (IOException | InterruptedException ioException) {
                    ioException.printStackTrace();
                }
            }
        });
        gridBagConstraints.gridwidth = 0;
        layout.addLayoutComponent(button, gridBagConstraints);
        frame.getContentPane().add(button);

        // 显示窗口
        frame.setVisible(true);

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                super.windowOpened(e);
                new Thread(() -> {
                    try {
                        Thread.sleep(10000);
                        button.setVisible(true);
                        frame.repaint();
                        frame.setVisible(true);
                    } catch (InterruptedException exception) {
                        exception.printStackTrace();
                    }
                }).start();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                // 窗口关闭后，主线程继续执行，恢复计时
                Optional.ofNullable(latch).ifPresent(CountDownLatch::countDown);
                super.windowClosed(e);
            }
        });
    }

    /**
     * 展示消息框
     *
     * @param message .
     */
    public static void showMessageBox(String message) {
        SwingUtilities.invokeLater(() -> {
            try {
                buildMessageBox(message);
            } catch (UnsupportedLookAndFeelException | ClassNotFoundException
                    | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        });
    }

    public static void main(String[] args) {
        String message = "<html><body><font size=\"6\">您已经持续工作%d分钟，请注意休息</font><br/><font size=\"4\">(10秒后锁屏)</font></body></html>";
        UI.showMessageBox(String.format(message, 45));
    }
}
