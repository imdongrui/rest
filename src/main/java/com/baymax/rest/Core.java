package com.baymax.rest;

import java.util.concurrent.CountDownLatch;

public class Core {

    public static void main(String[] args) {
        try {
            PropertiesManager manager = new PropertiesManager();
            int count = 1, interval = Integer.parseInt(manager.getProperty("system.core.interval"));
            System.out.printf("Rest已开始运行, 将每隔%d分钟提醒您休息\r\n", interval);
            while (true) {
                Thread.sleep(60000);
                System.out.printf("%d ", count);
                if (count >= interval) {
                    CountDownLatch latch = new CountDownLatch(1);
                    UI.setLatch(latch);
                    String message = "<html><body><font size=\"6\">您已经持续工作%d分钟，请注意休息</font><br/><font size=\"4\">(10秒后锁屏)</font></body></html>";
                    UI.showMessageBox(String.format(message, count));
                    latch.await();
                    count = 0;
                    System.out.println();
                }
                count++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
