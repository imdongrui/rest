package com.baymax.rest;

import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

public class SystemEvent {

    /**
     * 锁定屏幕
     *
     * @throws IOException          .
     * @throws InterruptedException .
     */
    public static void lockScreen() throws IOException, InterruptedException {
        Properties props = System.getProperties();
        String osName = props.getProperty("os.name").toLowerCase(Locale.ROOT);
        if (osName.contains("windows")) {
            Runtime runtime = Runtime.getRuntime();
            Process proc = runtime.exec("rundll32.exe user32.dll,LockWorkStation");
            proc.waitFor(); //容易造成主线程的阻塞。
        }
        if (osName.contains("linux")) {
            Runtime runtime = Runtime.getRuntime();
            Process proc = runtime.exec("dde-lock");
            proc.waitFor();
        }
    }
}
